package fr.starnakin.core.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.starnakin.core.Definer;

public class Msg implements CommandExecutor {

	private static ArrayList<Player> lastsender = new ArrayList<Player>();
	private static ArrayList<Player> blocker = new ArrayList<Player>();
	

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		
		if (sender instanceof Player) {
			
			Player p = (Player) sender;
			Player targetPlayer = Bukkit.getPlayer(args[0]);
			
			StringBuilder sb = new StringBuilder();
			
			for (int i = 1; i != args.length; i++) {
				sb.append(args[i] + " ");
			}
			if(cmd.getName().equalsIgnoreCase("blockmsg")) {
				
				if(args.length == 2) {
					
					if(targetPlayer != null) {
						
						
						if(args[1].contains("on")) {
							blocker.add(targetPlayer);
							p.sendMessage(ChatColor.GREEN + targetPlayer.getName() + " ne peut plus vous parler");								}
						
						if(args[1].contains("off")) {
						
							if(blocker != null) {
								blocker.add(targetPlayer);
								p.sendMessage(ChatColor.GREEN + targetPlayer.getName() + " ne peut vous parler");
							}
							
						} else {
							p.sendMessage(Definer.erreur + targetPlayer.getName()  + " peut d�j� vous parler !");
						}
						
					} else {
						p.sendMessage(Definer.erreur + "la commande est /Blockmsg Joueur on/off");
					}
				} else {
					p.sendMessage(Definer.erreur + "la commande est /Blockmsg Joueur on/off");
				}
			}
			
			if(cmd.getName().equalsIgnoreCase("msg")) {
			
				if(args.length >= 2) {
				
					if(targetPlayer != null) {
						
						if(p.getName() != targetPlayer.getName()) {
							
							if(!blocker.contains(p)) {
								
								lastsender.remove(p);
								lastsender.add(p);
								
								targetPlayer.sendMessage(ChatColor.GOLD + "[" + ChatColor.GRAY + p.getName() + ChatColor.GOLD + " -> " + ChatColor.GRAY + "Moi" + ChatColor.GOLD + "] " + ChatColor.RESET + sb.toString());
								p.sendMessage(ChatColor.GOLD + "[" + ChatColor.GRAY + "Moi" + ChatColor.GOLD + " -> " + ChatColor.GRAY + p.getName() + ChatColor.GOLD + "] " + ChatColor.RESET + sb.toString());
								
							} else {
								p.sendMessage(Definer.erreur + targetPlayer.getName() + " ne veut pas vous parler");
							}
							
						} else {
							p.sendMessage(Definer.erreur + args[0] + " n'est pas connect�");
						}
						
					} else {
						p.sendMessage(Definer.erreur + "vous ne pouvez pas parler tout(e) seul");
					}
					
				} else {
					p.sendMessage(Definer.erreur + "la commande est : /Msg pseudo message");
				}
			}
			
			if(cmd.getName().equalsIgnoreCase("repondre")) {
				
				if(lastsender != null){
					
					targetPlayer.sendMessage(ChatColor.GOLD + "[" + ChatColor.GRAY + p.getName() + ChatColor.GOLD + " -> " + ChatColor.GRAY + "Moi" + ChatColor.GOLD + "] " + ChatColor.RESET + sb.toString());
					p.sendMessage(ChatColor.GOLD + "[" + ChatColor.GRAY + "Moi" + ChatColor.GOLD + " -> " + ChatColor.GRAY + p.getName() + ChatColor.GOLD + "] " + ChatColor.RESET + sb.toString());
					
				}else {	
					p.sendMessage(Definer.erreur + "vous n'avez personne � qui r�pondre");
				}
				
			}
		}
		
		return false;
	}

}
