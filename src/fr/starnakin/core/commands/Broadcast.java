package fr.starnakin.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import fr.starnakin.core.Definer;

public class Broadcast implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(cmd.getName().equalsIgnoreCase("broadcast")) {
			
			if(args.length >= 1) {
				
				StringBuilder sb = new StringBuilder();
				
				for (int i = 0; i != args.length; i++) {
					sb.append(args[i] + " ");
				}
				
				Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + "{Broadcast} " + ChatColor.DARK_GREEN + sb.toString());
				
			}else {
				sender.sendMessage(Definer.erreur + "la commande est /broadcast message");
			}
		
		}
		return false;
	}

}
