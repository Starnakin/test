package fr.starnakin.core.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Vanish implements CommandExecutor {

	private static ArrayList<Player> vanished = new ArrayList<Player>();
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(cmd.getName().equalsIgnoreCase("vanish")) {
			
			Player p = (Player) sender;
			
			if (sender instanceof Player) {
				
				if(vanished.contains(p)){
					
					for(Player player : Bukkit.getOnlinePlayers()) {	player.showPlayer(p);	}
					vanished.remove(p);
					p.sendMessage(ChatColor.GREEN + "tu est visible par tout le monde");
				}else {					
					for(Player player : Bukkit.getOnlinePlayers()) {	player.hidePlayer(p);	}
					vanished.add(p);
					p.sendMessage(ChatColor.GREEN + "tu est invisible");
				}
				
				return true;
			}
		}
		return false;
	}

}
