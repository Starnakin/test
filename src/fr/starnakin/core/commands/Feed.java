package fr.starnakin.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Feed implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("feed")) {
			
			if(args.length == 1) {
				
	            Player targetPlayer = Bukkit.getPlayer(args[0]);
				
				if(targetPlayer != null) {
					targetPlayer.setFoodLevel(20);
					targetPlayer.sendMessage(ChatColor.GREEN + sender.getName() + " vous a nourrit");
				}
				
			} else {
				
				if (sender instanceof Player) {
					
					Player p = (Player) sender;
					
					p.setFoodLevel(20);
					p.sendMessage(ChatColor.GREEN +"vous �tes nourrit");
				}
				
			}

			
		}
			
		return false;
	}

}
