package fr.starnakin.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Heal implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("heal")) {
			
			if(args.length == 1) {
				
	            Player targetPlayer = Bukkit.getPlayer(args[0]);
				
				if(targetPlayer != null) {
					targetPlayer.setHealth(targetPlayer.getMaxHealth());
					targetPlayer.sendMessage(ChatColor.GREEN + sender.getName() + " vous a soign�");
				}
				
			} else {
				
				if (sender instanceof Player) {
					
					Player p = (Player) sender;
					
					p.setHealth(p.getMaxHealth());
					p.sendMessage(ChatColor.GREEN +"vous �tes soign�");
				}
				
			}

			
		}
			
		return false;
	}

}
