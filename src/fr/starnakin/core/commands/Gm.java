package fr.starnakin.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.starnakin.core.Definer;
import fr.tarkod.api.getGuiManager;

public class Gm implements CommandExecutor {

	public static Inventory inv = Bukkit.createInventory(null, 9, ChatColor.BLUE + "Gamemode Selector");
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(cmd.getName().equalsIgnoreCase("gm")) {
			
			if (sender instanceof Player) {
								
				Player p = (Player) sender;

				
				if (sender instanceof Player) {
					
					
					if(args.length == 0) {
						
						getGuiManager.setSlotData(inv, new ItemStack(Material.DIAMOND_SWORD), 1, ChatColor.GRAY + "SURVIVAL");
						getGuiManager.setSlotData(inv, new ItemStack(Material.GOLD_BLOCK), 3, ChatColor.GRAY + "CREATIVE");
						getGuiManager.setSlotData(inv, new ItemStack(Material.APPLE), 5, ChatColor.GRAY + "ADVENTURE");
						getGuiManager.setSlotData(inv, new ItemStack(Material.GLASS), 7, ChatColor.GRAY + "SPECTATOR");
						
						p.openInventory(inv);
						
					}
				
					if(args.length == 1) {
						
						String message = ChatColor.GREEN + "vous venez de passer en Gamemode " + p.getGameMode();
								
						if(args[0].contains("0")) {	
							p.setGameMode(GameMode.SURVIVAL);
							p.sendMessage(message);
						}
						if(args[0].contains("1")) {
							p.setGameMode(GameMode.CREATIVE);
							p.sendMessage(message);
						}
						if(args[0].contains("2")) {
							p.setGameMode(GameMode.ADVENTURE);
							p.sendMessage(message);
						}
						if(args[0].contains("3")) {
							p.setGameMode(GameMode.SPECTATOR);
							p.sendMessage(message);
						}
						if(args[0].contains("0") || args[0].contains("1") || args[0].contains("2") || args[0].contains("3")) {
							p.sendMessage(Definer.erreur + "la commands s'utilise /gamemode MODE");							
						}

					}
						
					if(args.length >= 2) {
						
						Player targetPlayer = Bukkit.getPlayer(args[0]);
						if(targetPlayer != null) {
							String message = ChatColor.GREEN + p.getName() +" vien de vous passer en Gamemode " + targetPlayer.getGameMode();
							String message2 = ChatColor.GREEN + "vous avez pass� " + targetPlayer.getName() + " en Gamemode " + targetPlayer.getGameMode();
							
							if(args[1].contains("0")) {	
								targetPlayer.setGameMode(GameMode.SURVIVAL);
								targetPlayer.sendMessage(message);
								p.sendMessage(message2);
							}
							if(args[1].contains("1")) {
								targetPlayer.setGameMode(GameMode.CREATIVE);
								targetPlayer.sendMessage(message);
								p.sendMessage(message2);;
							}
							if(args[1].contains("2")) {
								targetPlayer.setGameMode(GameMode.ADVENTURE);
								targetPlayer.sendMessage(message);
								p.sendMessage(message2);
							}
							if(args[1].contains("3")) {
								targetPlayer.setGameMode(GameMode.SPECTATOR);
								targetPlayer.sendMessage(message);
								p.sendMessage(message2);
							}
							
						}else {
							p.sendMessage(args[0] + " n'est pas connect�");
						}
					}
							
				}
			}
		}
		return false;
	}
}
