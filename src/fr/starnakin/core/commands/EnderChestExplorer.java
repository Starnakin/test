package fr.starnakin.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EnderChestExplorer implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (sender instanceof Player) {
			
			Player p = (Player) sender;
			
			if(cmd.getName().equalsIgnoreCase("enderchest")) {
				
				Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
				
				if(Bukkit.getOnlinePlayers().contains(targetPlayer)) {
					
					p.openInventory(targetPlayer.getEnderChest());
				
				} else {
					
					p.openInventory(p.getEnderChest());
					
				}
			
			}
			
		}
		return false;
	}

}