package fr.starnakin.core;

import org.bukkit.plugin.java.JavaPlugin;

import fr.starnakin.core.commands.Broadcast;
import fr.starnakin.core.commands.EnderChestExplorer;
import fr.starnakin.core.commands.Feed;
import fr.starnakin.core.commands.Gm;
import fr.starnakin.core.commands.Heal;
import fr.starnakin.core.commands.Holograme;
import fr.starnakin.core.commands.InventoryExplorer;
import fr.starnakin.core.commands.Msg;
import fr.starnakin.core.commands.Spawn;
import fr.starnakin.core.commands.Vanish;
import fr.starnakin.core.listeners.GmGui;
import fr.starnakin.core.listeners.Join;



public class Main extends JavaPlugin {

	@Override
	public void onEnable() {
		System.out.println("the server was enable");
		getCommand("gm").setExecutor(new Gm());
		getCommand("inv").setExecutor(new InventoryExplorer());
		getCommand("enderchest").setExecutor(new EnderChestExplorer());
		getCommand("msg").setExecutor(new Msg());
		getCommand("reply").setExecutor(new Msg());
		getCommand("blockmsg").setExecutor(new Msg());
		getCommand("broadcast").setExecutor(new Broadcast());
		getCommand("vanish").setExecutor(new Vanish());
		getCommand("spawn").setExecutor(new Spawn());
		getCommand("heal").setExecutor(new Heal());
		getCommand("feed").setExecutor(new Feed());
		getCommand("holograme").setExecutor(new Holograme());
		
		
		getServer().getPluginManager().registerEvents(new GmGui(), this);
		getServer().getPluginManager().registerEvents(new Join(), this);
	}
}
