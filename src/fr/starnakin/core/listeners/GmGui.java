package fr.starnakin.core.listeners;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.starnakin.core.commands.Gm;

public class GmGui implements Listener {

    @EventHandler
    public void onClickonTeamGUI(InventoryClickEvent e) {
    	
        Player p = (Player) e.getWhoClicked();
        if(e.getClickedInventory().getName() == Gm.inv.getName()) {
        	
			e.setCancelled(true);
			
			String message = ChatColor.GREEN + "vous êtes passé en " + p.getGameMode();
			
			if(e.getCurrentItem().getType() == Material.DIAMOND_SWORD) {
				p.setGameMode(GameMode.SURVIVAL);
				p.sendMessage(message);
				p.closeInventory();
			}
			
			if(e.getCurrentItem().getType() == Material.GOLD_BLOCK) {
				p.setGameMode(GameMode.CREATIVE);
				p.sendMessage(message);
				p.closeInventory();
			}
			
			if(e.getCurrentItem().getType() == Material.APPLE) {
				p.setGameMode(GameMode.ADVENTURE);
				p.sendMessage(message);
				p.closeInventory();
			}
			
			if(e.getCurrentItem().getType() == Material.GLASS){
				p.setGameMode(GameMode.SPECTATOR);
				p.sendMessage(message);
				p.closeInventory();
				
			}
        }		

	}

}
