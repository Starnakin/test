package fr.starnakin.core.listeners;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Join implements Listener {
	
	@SuppressWarnings("deprecation")
	public void onJoin(PlayerJoinEvent e) {
		
		Player p = e.getPlayer();
		
		Location spawn = new Location(p.getWorld(), 100.5, 142, 100.5);
		spawn.getBlock().setType(Material.COBBLESTONE);
		Location spawnPlayer = spawn.add(0, 1, 0);
		p.teleport(spawnPlayer);
		
        p.getInventory().clear();
        p.setGameMode(GameMode.ADVENTURE);
        p.setHealth(p.getMaxHealth());
        p.setFoodLevel(20);
        p.setSaturation(20);
	
	}

}
